<!DOCTYPE html>
<html>
	<head>
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
	</head>
	<body>
		<?php
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_menu();
		
			$clase = "'jumbotron'";
			@session_start();
			
			echo '<div class="jumbotron">
				<h1>Sistema de Taller</h1>
				<p>Bienvenido al Sistema de Taller "' . $_SESSION['nombre'] . '"</p>
				<p>
				<a class="btn btn-lg btn-primary" role="button" onclick="cerrar(' . $clase . 
				')">Cerrar</a>
				</p>
				</div>';
		?>
	</body>
</html>