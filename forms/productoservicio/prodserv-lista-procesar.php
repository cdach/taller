<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/ProductoServicio.php");
	
	$producto = new ProductoServicio();
	$rs = $producto->listarProductosServicios();
	
	echo '<table id="lista-datos" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>Medida</th>
                <th>Clasif.</th>
                <th>Marca</th>
                <th>Precio Vta.</th>
                <th>IVA</th>
                <th>Habilitado</th>
                <th>ID</th>
                <th>Acciones</th>
            </tr>
        </thead>
		<tbody>';
	if($rs){
		foreach($rs as $fila){
			
			$iva = $fila["iva"];
			if($iva == "0") $iva = "Exento";
			elseif($iva == "1") $iva = "IVA 5%";
			elseif($iva == "2") $iva = "IVA 10%";
			
			echo "<tr>
					<td>" . $fila["codigo"] . "</td>
					<td>" . $fila["nombre"] . "</td>
					<td>" . $fila["nombre_unidad"] . "</td>
					<td>" . $fila["nombre_clasificacion"] . "</td>
					<td>" . $fila["nombre_marca"] . "</td>
					<td>" . number_format($fila["precio_venta"],0,"",".") . "</td>
					<td>" . $iva . "</td>
					<td>" . $fila["habilitado"] . "</td>
					<td>" . $fila["id_servicio_producto"] . "</td>
					<td>
						<a class='btn btn-warning btn-sm' href='prodserv-editar.php?id=" . $fila["id_servicio_producto"] . "'
						data-toggle='tooltip' title='Editar'>
						<i class='fa fa-edit'></i></a>
						<a class='btn btn-danger btn-sm' onclick='borrarProductoServicio(" . '"' . $fila["id_servicio_producto"] . '"' . ");'
						data-toggle='tooltip' title='Borrar'>
						<i class='fa fa-trash'></i></a>	
					</td>
				 </tr>";
		}
	}
		
	echo '</tbody>
	</table>';
	
	fn_setear_datatable("lista-datos");
		
	unset($unidad);