<?php
	include_once("../../lib/funciones.php");
	
	$id_servicio_producto 	= $_POST["id_servicio_producto"];
	$codigo 				= $_POST["codigo"];
	$nombre 				= $_POST["nombre"];
	$descripcion 			= $_POST["descripcion"];
	$id_clasificacion		= $_POST["id_clasificacion"];
	$id_unidad 				= $_POST["id_unidad"];
	$id_marca 					= $_POST["id_marca"];
	$precio_venta 			= $_POST["precio_venta"];
	$habilitado 			= $_POST["habilitado"];
	$iva 					= $_POST["iva"];
	
	include_once("prodserv-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/ProductoServicio.php");	
	$producto = new ProductoServicio($id_servicio_producto, $codigo,$nombre,$descripcion,
									 $id_clasificacion,$id_unidad,$id_marca,$precio_venta,$habilitado,$iva);
	
	if($producto->editarProductoServicio()){
		unset($producto);
		echo "<script> location.href='prodserv-lista.php';</script>";
	}
?>