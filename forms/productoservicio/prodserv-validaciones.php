<?php
	$controlError = true;
	
	echo "<script>
			$('#codigo_ayuda').html('');
			$('#nombre_ayuda').html('');
			$('#descripcion_ayuda').html('');
			$('#id_clasificacion_ayuda').html('');
			$('#id_unidad_ayuda').html('');
			$('#id_marca_ayuda').html('');
			$('#precio_venta_ayuda').html('');
			$('#habilitado_ayuda').html('');
			$('#iva_ayuda').html('');
		 </script>";
			
	if($codigo == ""){
		echo "<script> $('#codigo_ayuda').html('Debe ingresar el código!!!'); 
		$('#codigo').focus(); </script>";
		$controlError = false;
		return;
	}
		
	if($nombre == ""){
		echo "<script> $('#nombre_ayuda').html('Debe ingresar el nombre!!!'); 
		$('#nombre').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($id_clasificacion == ""){
		echo "<script> $('#id_clasificacion_ayuda').html('Debe ingresar la clasificaion!!!'); 
		$('#id_clasificacion').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($id_unidad == ""){
		echo "<script> $('#id_unidad_ayuda').html('Debe ingresar la unidad!!!'); 
		$('#id_unidad').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($id_marca == ""){
		echo "<script> $('#id_marca_ayuda').html('Debe ingresar la marca!!!'); 
		$('#id_marca').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if(!fn_validar_dato($precio_venta,"integer") || $precio_venta < 0){
		echo "<script> $('#precio_venta_ayuda').html('Debe ingresar un precio de venta correcto!!!'); 
		$('#precio_venta').focus(); </script>";
		$controlError = false;
		return;
	}
?>