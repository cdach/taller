<?php
	
	include_once("../../lib/funciones.php");
	include_once("../../clases/Conexion.php");
	$conn = new Conexion();
		
	$usuario = $_POST["usuario"];
	$clave = $_POST["clave"];
	
	if($usuario == ""){
		fn_mensaje("Faltan Datos","Ingrese el usuario",true);
		return;
	}
	
	if($clave == ""){
		fn_mensaje("Faltan Datos","Ingrese la clave",true);
		return;
	}
	
	$clave = sha1($clave);
	
	$sql = "select 	estado, perfil, nombre
			from 	usuarios
			where	id_usuario = '$usuario'
			and		clave = '$clave'";
		
	$row = $conn->consultarSql($sql);
	
	if(!$row){
		fn_mensaje("Error en Conexión","Usuario " . $usuario . " no existe o la clave es incorrecta.",true); 
		return;
	}
		
	if($row[0]["estado"] != "A"){
		fn_mensaje("Error en Conexión","El usuario no está Activo. Verifique");
		return;
	}
	
	// Datos de la Sesión
	session_start();
	$_SESSION["usuario"] = $usuario;
	$_SESSION["estado"] = $row[0]["estado"];
	$_SESSION["perfil"] = $row[0]["perfil"];
	$_SESSION["nombre"] = $row[0]["nombre"];
		
	$conn->desconectar();
	
	echo "<script> location.href= '../menu/menu.php'; </script>";
	