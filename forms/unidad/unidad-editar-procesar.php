<?php
	include_once("../../lib/funciones.php");
	
	$id_unidad 		= $_POST["id_unidad"];
	$nombre 		= $_POST["nombre"];
	$sigla 			= $_POST["sigla"];
	
	include_once("unidad-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Unidad.php");	
	$unidad = new Unidad($id_unidad,$nombre,$sigla);
		
	if($unidad->editarUnidad()){
		unset($unidad);
		echo "<script> location.href='unidad-lista.php';</script>";
	}
?>