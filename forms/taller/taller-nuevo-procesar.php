<?php
	include_once("../../lib/funciones.php");
	
	$nombre 			= $_POST["nombre"];
	$direccion 			= $_POST["direccion"];
	$localidad 			= $_POST["localidad"];
	$telefono_principal = $_POST["telefono_principal"];
	$telefono_secundario = $_POST["telefono_secundario"];
	$ruc 				= $_POST["ruc"];
	$email 				= $_POST["email"];
	$estado 			= $_POST["estado"];
	
	include_once("taller-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Taller.php");
	$taller = new Taller("",$nombre,$direccion,$localidad,
	$telefono_principal,$telefono_secundario,$ruc,$email,$estado);
		
	if($taller->grabarTaller()){
		unset($taller);
		echo "<script> location.href='taller-lista.php';</script>";
	}
?>