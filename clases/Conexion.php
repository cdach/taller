<?php
	// namespace taller\clases;
	
	class Conexion{
		private $host;
		private $dbname;
		private $user;
		private $password;
		private $dbms;
		private $conexion;
		
		// Constructor
		function __construct(){
			$ini = "../../clases/configuracion.ini";
			
			if(!file_exists($ini)){
				fn_mensaje("Error en Conexión","No se encuentra " . $ini);
				return;
			}
			
			$parametros = parse_ini_file($ini,true);
			$this->host = $parametros["conexion"]["host"];
			$this->dbname = $parametros["conexion"]["dbname"];
			$this->user = $parametros["conexion"]["user"];
			$this->password = $parametros["conexion"]["password"];
			$this->dbms = $parametros["conexion"]["dbms"];
			
			$string_conexion = $this->dbms . ":host=" . $this->host . ";dbname=" . $this->dbname;
			
			try
			{
				$this->conexion = new PDO($string_conexion,$this->user,$this->password);
				$this->conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			catch(PDOException $e)
			{
				fn_mensaje("Error en Conexión", $e->getMessage());
				return;
			}
		}
		
		// Consulta
		function consultarSql($sql,$select=true)
		{
			try
			{
				$sentencia = $this->conexion->prepare($sql);
				$sentencia->setFetchMode(PDO::FETCH_BOTH);
				$resultadoSentencia = $sentencia->execute();
				if($select)
				{
					$row = $sentencia->fetchAll();
				
					if(count($row) > 0)
						return $row;
					else
						return false;
				}
				else
					return $resultadoSentencia;
			}
			catch(PDOException $e)
			{
				fn_mensaje("Error en Sql",$e->getMessage());
			}
		}
		
		function getConexion(){
			return $this->conexion;
		}
		
		// Desconecta de la Base de Datos
		function desconectar(){
			$this->conexion = null;
		}
	}