<?php
	include_once("Conexion.php");
	
	class Marca{
		private $id_marca;
		private $nombre;
		
		
		function __construct($id_marca="",$nombre=""){
			$this->id_marca = $id_marca;
			$this->nombre = $nombre;
			$this->conexion = new Conexion();
		}
			
		function __destruct(){
			$this->conexion = null;
		}
			
		// Métodos Getters
		function getIdMarca(){
			return $this->id_marca;
		}
			
		function getNombre(){
			return $this->nombre;
		}
		
		function setIdMarca($id_clasificacion){
			$this->id_clasificacion = $id_clasificacion;
		}
			
		function setNombre($nombre){
			$this->nombre = $nombre;
		}
		
		function recuperarClasificacion($id){
			$sql = "select * from clasificaciones where id_clasificacion = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_clasificacion = $fila[0]["id_clasificacion"];
				$this->nombre = $fila[0]["nombre"];
				}
		}
		
		function listarClasificacion(){
			$sql = "select id_clasificacion, nombre
			from clasificaciones order by id_clasificacion";
				
			$rs = $this->conexion->consultarSql($sql);
			return $rs;
		}

		function grabarClasificacion(){
			$sql = "insert into clasificaciones(nombre)
			values('$this->nombre')";
			
			return $this->conexion->consultarSql($sql,false);
		}
			
		// Editar el registro
		function editarClasificacion(){
			$sql = "update clasificaciones
			set nombre = '$this->nombre'
			where id_clasificacion = '$this->id_clasificacion'";
				
			return $this->conexion->consultarSql($sql,false);
		}
			
			// Borrar el registro
		function borrarClasificacion($id){
			$sql = "delete from clasificaciones where id_clasificacion = '" . $id . "'";
			
			return $this->conexion->consultarSql($sql,false);
		}
	}
?>